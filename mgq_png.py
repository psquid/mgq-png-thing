#!/usr/bin/env python


def masked_pixel(base_pixel, mask_pixel):
    return (
            base_pixel[0],
            base_pixel[1],
            base_pixel[2],
            255 - mask_pixel[0]
            )


def png_data_from_bmp_data(bmp_data):
    from PIL import Image
    from io import BytesIO

    bmp_image = Image.open(bmp_data)

    if bmp_image.format != 'BMP':
        raise ValueError('Input file was not a valid bitmap.')

    bmp_grid = bmp_image.load()
    png_image = Image.new('RGBA', (bmp_image.size[0] // 2, bmp_image.size[1]))
    png_grid = png_image.load()

    for x in range(png_image.size[0]):
        for y in range(png_image.size[1]):
            png_grid[x, y] = masked_pixel(bmp_grid[x, y],
                    bmp_grid[x+png_image.size[0], y])

    png_data = BytesIO()

    png_image.save(png_data, format='PNG')

    png_data.seek(0)
    return png_data.read()


def process_bmp(bmp_file, clobber=False, verbose=False):
    import os.path, sys

    out_file_name = bmp_file.name
    if out_file_name[-4:].lower() == '.bmp':
        out_file_name = out_file_name[:-4]
    out_file_name += '.png'

    if os.path.exists(out_file_name):
        if not os.path.isfile(out_file_name):
            sys.stderr.write(('"{}" exists but is not a regular file, safe ' +
                'overwriting cannot be guaranteed. ' +
                'Skipping.\n').format(out_file_name))
            bmp_file.close()
            return False

        if clobber and verbose:
            sys.stdout.write(('Output file "{}" already exists, ' +
                    'overwriting.\n').format(out_file_name))
        elif not clobber:
            response = input(('Output file "{}" already exists, ' +
                    'overwrite [y/N]? ').format(out_file_name)).lower() or 'n'
            if response not in ['y', 'yes']:
                if verbose:
                    sys.stdout.write('Skipped input "{}".\n'.format(bmp_file.name))
                bmp_file.close()
                return False

    with open(out_file_name, 'wb') as out_file:
        try:
            out_file.write(png_data_from_bmp_data(bmp_file))
            bmp_file.close()
            return True
        except (ValueError, IOError) as e:
            sys.stderr.write('Couldn\'t process "{}": {}\n'.format(bmp_file.name,
                    e.args[0]))
            bmp_file.close()
            return False


def main():
    import argparse, sys

    if sys.version_info < (3, 0):
        sys.stderr.write('This is a Python 3 script. Please run it under ' +
                'a Python 3 interpreter.\n')
        sys.exit(1)

    parser = argparse.ArgumentParser(description='''Convert Monster Girl Quest
            masked bitmaps to PNGs.''')
    parser.add_argument('in_files', metavar='FILE', nargs='+', help='''A file to
            process. Output file will be the same name, with a .bmp suffix
            removed if present, and a .png suffix added.''',
            type=argparse.FileType('rb'))
    parser.add_argument('--clobber', help='''Overwrite output files without
            asking.''', action='store_true')
    parser.add_argument('-v', '--verbose', help='''Print additional status
            messages.''', action='store_true')

    args = parser.parse_args()

    failures = []

    for in_file in args.in_files:
        if not process_bmp(in_file, clobber=args.clobber, verbose=args.verbose):
            failures.append(in_file.name)

    if failures != []:
        if args.verbose:
            sys.stdout.write('\nThe following files failed to process ' +
                    'correctly:\n')
            for failed_file_name in failures:
                sys.stdout.write('\t{}\n'.format(failed_file_name))
        sys.exit(1)
    else:
        sys.exit()

if __name__ == '__main__':
    main()
